import React from "react";
import './App.css';
import {Home, WhyUs, Testimonial, GettingStarted, Faq, Car} from "./pages";
// import {Navbar, Nav, Container} from "react-bootstrap";
import { NavbarComp, Services, FooterComp } from "../src/components";
import { BrowserRouter, Routes, Route, Link} from "react-router-dom";

function App() {
  return (
    <div>
      <NavbarComp />
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/car" element={<Car />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
