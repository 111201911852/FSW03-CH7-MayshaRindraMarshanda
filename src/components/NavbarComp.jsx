import React from 'react';
// import {Container,Nav} from 'react-bootstrap'
import "../styles/index.css";


function NavbarComp() {
    return ( 
        <div  className="navbar navbar-expand-lg fixed-top ">
          <div className="container navbar-bg">
            <a className="navbar-brand" href="index.html">BinarRental</a>
            <button className = "navbar-toggler border-0" type = "button" data-bs-toggle = "collapse" 
            data-bs-target = "#navMenu">
              <span className = "navbar-toggler-icon"></span>
            </button>
            <div className = "collapse navbar-collapse" id = "navMenu">
              <ul className = "navbar-nav ms-auto">
                <li className = "nav-item">
                  <a className = "nav-link" href="#konten">Our Services</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#why">Why Us</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#testimonial">Testimonial</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#faq">FAQ</a>
                </li>
                <li className="nav-item"><button className=" nav-link btn-regis">Register</button></li>
              </ul>
            </div>
          </div>
        </div>
        

    )
}

export default NavbarComp;
