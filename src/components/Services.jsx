import React from 'react';
import people from '../assets/people.png';

function Services() {
    return (
        <section id="konten">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="box-1"> </div>
                        <div class="box-2"> </div>
                        <div class="konten-bg2">
                            <div class="box-3"> </div>
                            <img src={people} alt="BinarRental"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="konten-tulisan">
                            <h1>Best Car Rental for any kind of trip in (Lokasimu)!</h1>
                            <p>Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain,
                            kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.</p><br/>
                            <div class="row konten-centang py-3">
                                <div class="col text">
                                    <i class="bi bi-check rounded-circle"></i>
                                    <span>Sewa Mobil Dengan Supir di Bali 12 Jam</span><br/><br/>
                                    <i class="bi bi-check rounded-circle"></i>
                                    <span>Sewa Mobil Lepas Kunci di Bali 24 Jam</span> <br/><br/>
                                    <i class="bi bi-check rounded-circle"></i>
                                    <span>Sewa Mobil Jangka Panjang Bulanan</span><br/><br/>
                                    <i class="bi bi-check rounded-circle"></i>
                                    <span>Gratis Antar - Jemput Mobil di Bandara</span><br/><br/>
                                    <i class="bi bi-check rounded-circle"></i>
                                    <span>Layanan Airport Transfer / Drop In Out</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
  )
}

export default Services;