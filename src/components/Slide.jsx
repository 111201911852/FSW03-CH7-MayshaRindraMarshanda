import React from 'react';
import mobil from '../assets/mobil.png';
import { Link } from "react-router-dom";

function Slide() {
  return (
    <div>
        <section class="services d-flex align-items-center">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-7">
                <div class="services-text">
                  <h1>Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)</h1>
                  <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Corporis voluptatibus deleniti quasi nemo neque quas id ipsum impedit, enim quod unde earum, totam dolore odit inventore! Vero fugiat atque error!</p>
                  <div class="tombol">
                    <Link to="/Car" class="btn-regis shadow">Mulai Sewa Mobil</Link>
                  </div>
                </div>
              </div>
              <div class="col-md-5">
                <div class="services-bg"></div>
                <img class="car" src={mobil} alt="BinarRental"/>
              </div>
            </div>
          </div>
        </section>
    </div>
  )
}

export default Slide