import React from 'react';
import { Link} from "react-router-dom";

function FooterComp() {
    return (
        // <!-------------------- FOOTER START   ------------------------->
        <footer class="bg-white text-black pt-5 pb-4">
          <div class="container text-md-left">
            <div class="row text-md-left">
              <div class= "col-md-3 col-lg-3 col-xl-3 mx-auto mt-3">
                <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
                <p>binarcarrental@gmail.com</p>
                <p>081-233-334-808</p>
              </div>
              <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mt-3">
                <p><a href="#konten" class="text-black">Our services</a></p>
                <p><a href="#why" class="text-black">Why Us</a></p>
                <p><a href="#testimonial" class="text-black">Testimonial</a></p>
                <p><a href="#faq" class="text-black">FAQ</a></p>
              </div>
              <div class="col-md-4 col-xl-2 mx-auto mt-3">
                <p>Connect with us</p>
                <div class="icon-footer rounded md-5">
                  <i class="bi bi-facebook"></i>
                  <i class="bi bi-instagram"></i>
                  <i class="bi bi-twitter"></i>
                  <i class="bi bi-envelope"></i>
                  <i class="bi bi-twitch"></i>
                </div>
              </div>
              <div class="col-md-4 col-xl-3 mx-auto mt-3">
                <p>Copyright Binar 2022</p>
                <a class="navbar-brand" href="index.html">BinarRental</a>
              </div>
            </div>
          </div>
        </footer>
    )
}

export default FooterComp;
