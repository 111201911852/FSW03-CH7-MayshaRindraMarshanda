import React from "react";
import { NavbarComp, Slide} from "../components";
function Car() {
    return (
        <div>
            <NavbarComp/>
            <Slide/>
            <section id="cars">
                <div class="container-lg isi-cars">
                    <div class="row ">
                        <div class="col-md-3">
                            <h3 class="tulis-cars">Tipe Driver</h3>
                            <div class="dropdown ">
                                <button class="btn dropdowncars" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                Pilih Tipe Driver <i class="bi bi-caret-down"></i>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                    <li><a class="dropdown-item">Dengan Sopir</a></li>
                                    <li><a class="dropdown-item">Tanpa Sopir (Lepas Kunci)</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <h3 class="tulis-cars">Tanggal</h3>
                            <div class="dropdown ">
                                <button class="btn dropdowncars" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                Pilih Tanggal <i class="bi bi-calendar"></i>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                    <input type="date"/>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <h3 class="tulis-cars">Waktu Jemput/Ambil</h3>
                            <div class="dropdown ">
                                <button class="btn dropdowncars" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                Pilih Waktu <i class="bi bi-caret-down"></i>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                    <li><a class="dropdown-item" href="#">06.00</a></li>
                                    <li><a class="dropdown-item" href="#">09.00</a></li>
                                    <li><a class="dropdown-item" href="#">10.00</a></li>
                                    <li><a class="dropdown-item" href="#">11.00</a></li>
                                    <li><a class="dropdown-item" href="#">12.00</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <h3 class="tulis-cars">Jumlah Penumpang(Optional)</h3>
                            <div class="form-input">
                                <input type="text" name="penumpang" id="penumpang" placeholder="Jumlah Penumpang"/>
                                <span class="iconpenumapang"><i class="bi bi-people-fill"></i></span>
                            </div> 
                        </div>
                        <div class="col-md-3">
                            <button id="load-btn" class="btn-regis shadow">Cari Mobil</button>
                        </div>
                        <div id="cars-container">
                            {/* RESULT */}
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
}

export default Car;