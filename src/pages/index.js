export { default as Home } from './Home';
export { default as WhyUs } from './WhyUs';
export { default as Testimonial } from './Testimonial';
export { default as GettingStarted } from './GettingStarted';
export { default as Faq } from './Faq';
export { default as Car } from './Car';