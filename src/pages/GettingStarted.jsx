import React from 'react';

function GettingStarted() {
    return (
        // <!----------Getting Started Section STARTS ----------------->
        <section class="started-section">
          <div class="container-lg py-4">
            <div class="row justify-content-center">
              <div class="col-lg-9 text-center">
                <h1 class="text-light started-text">Sewa Mobil di Solo Sekarang</h1>
                <p class="text-2 mb-5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Rem quos placeat sequi cum fugiat quia quo ducimus hic, accusamus sed delectus. Fuga optio aut minus corrupti sed temporibus quidem quo?</p>
                <div class="tombol">
                  <a href="#" class="btn-regis shadow tulisan-sedang">Mulai Sewa Mobil</a>
                </div>
              </div>
            </div>
          </div>
        </section>
        // <!----------Getting Started Section ENDS ----------------->

    )
}

export default GettingStarted;
