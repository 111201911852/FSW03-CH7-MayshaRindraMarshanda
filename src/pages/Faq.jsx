import React from 'react';

function Faq() {
    return (
        // <!-------------------- FAQ STARTS ------------------------->
        <section class="faq d-flex" id="faq">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-5">
                        <div class="faq-text">
                            <h1>Frequently Asked Question</h1>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                        </div>
                    </div>
                    
                    <div class="col-md-6 faq-btn">
                        <form>
                            <select class="form-select" aria-label="Default select example">
                                <option selected>Apa saja syarat yang dibutuhkan?</option>
                                <option value="1">Lorem ipsum dolor sit amet consectetur adipisicing elit.</option>
                                <option value="2">Lorem ipsum dolor sit amet consectetur adipisicing elit.</option>
                                <option value="3">Lorem ipsum dolor sit amet consectetur adipisicing elit.</option>
                            </select>
                            <br/>
                            <select class="form-select" aria-label="Default select example">
                                <option selected>Berapa hari minimal sewa mobil lepas kunci?</option>
                                <option value="1">Lorem ipsum dolor sit amet consectetur adipisicing elit.</option>
                                <option value="2">Lorem ipsum dolor sit amet consectetur adipisicing elit.</option>
                                <option value="3">Lorem ipsum dolor sit amet consectetur adipisicing elit.</option>
                            </select>
                            <br/>
                            <select class="form-select" aria-label="Default select example">
                                <option selected>Berapa hari sebelumnya sabaiknya booking sewa mobil?</option>
                                <option value="1">Lorem ipsum dolor sit amet consectetur adipisicing elit.</option>
                                <option value="2">Lorem ipsum dolor sit amet consectetur adipisicing elit.</option>
                                <option value="3">Lorem ipsum dolor sit amet consectetur adipisicing elit.</option>
                            </select>
                            <br/>
                            <select class="form-select" aria-label="Default select example">
                                <option selected>Apakah Ada biaya antar-jemput?</option>
                                <option value="1">Lorem ipsum dolor sit amet consectetur adipisicing elit.</option>
                                <option value="2">Lorem ipsum dolor sit amet consectetur adipisicing elit.</option>
                                <option value="3">Lorem ipsum dolor sit amet consectetur adipisicing elit.</option>
                            </select>
                            <br/>
                            <select class="form-select" aria-label="Default select example">
                                <option selected>Bagaimana jika terjadi kecelakaan</option>
                                <option value="1">Lorem ipsum dolor sit amet consectetur adipisicing elit.</option>
                                <option value="2">Lorem ipsum dolor sit amet consectetur adipisicing elit.</option>
                                <option value="3">Lorem ipsum dolor sit amet consectetur adipisicing elit.</option>
                            </select>
                        </form>
                    </div>
                </div>
            </div>
        </section>
  
    )
}

export default Faq;
