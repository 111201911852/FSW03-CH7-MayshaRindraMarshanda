import React from 'react';
import peoplecircle1 from '../assets/peoplecircle1.png';
import peoplecircle2 from '../assets/peoplecircle2.png';
function Testimonial() {
    return (
        // <!----------Testimonial  STARTS ----------------->
        <section id="testimonial">
            <div class="container-fluid ">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="testi-text text-center">
                            <h2 class="fw-bold mb-4">Testimonial</h2>
                            <p>Berbagai review positif dari para pelanggan kami</p>
                        </div>
                    </div>
                </div>

                <div class="row testimonial-1">
                    <div class="col col-xl-12 p-5">
                        <div id="carousel1" class="carousel slide" data-bs-ride="carousel">                           
                            <div class="carousel-indicators">
                                <button type="button" data-bs-target="#carousel1" data-bs-slide-to="0" class="active bg-success" aria-current="true" aria-label="Slide 1">
                                </button>
                                <button type="button" data-bs-target="#carousel1" data-bs-slide-to="1" aria-label="Slide 2" class="bg-success">
                                </button>
                            </div>
                            <div class="carousel-inner">
                                {/* <!-----------------Testi item start-------------------> */}
                                <div class="carousel-item active shadow-sm p-4">
                                    <div class="row">
                                        <div class="col-md-6 testi-konten d-flex">
                                            <img src= {peoplecircle1} alt=""/>
                                            <div class="konten-icon ms-4">
                                                <div class="rating text-warning">
                                                    <i class="bi bi-star-fill"></i>
                                                    <i class="bi bi-star-fill"></i>
                                                    <i class="bi bi-star-fill"></i>
                                                    <i class="bi bi-star-fill"></i>
                                                    <i class="bi bi-star-fill"></i>
                                                </div>
                                                <p class="text-muted">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Itaque reprehenderit accusamus, iste cupiditate quae commodi necessitatibus hic obcaecati sunt recusandae. Tenetur ex veritatis est in sit aspernatur, quasi molestias nam?</p>
                                                <h4>John Dee 32, Bromo</h4>
                                            </div>
                                        </div>

                                        <div class="col-md-5 testi-konten d-flex">
                                            <img src={peoplecircle1} alt=""/>
                                            <div class="konten-icon ms-4">
                                                <div class="rating text-warning">
                                                    <i class="bi bi-star-fill"></i>
                                                    <i class="bi bi-star-fill"></i>
                                                    <i class="bi bi-star-fill"></i>
                                                    <i class="bi bi-star-fill"></i>
                                                    <i class="bi bi-star-fill"></i>
                                                </div>
                                                <p class="text-muted">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Itaque reprehenderit accusamus, iste cupiditate quae commodi necessitatibus hic obcaecati sunt recusandae. Tenetur ex veritatis est in sit aspernatur, quasi molestias nam?</p>
                                                <h4>John Dee 25, Bromo</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {/* <!-----------------Testi item start-------------------> */}
                                <div class="carousel-item shadow-sm p-4">
                                    <div class="row">
                                        <div class="col-md-6 testi-konten d-flex">
                                            <img src={peoplecircle2} alt=""/>
                                            <div class="konten-icon ms-4">
                                                <div class="rating text-warning">
                                                    <i class="bi bi-star-fill"></i>
                                                    <i class="bi bi-star-fill"></i>
                                                    <i class="bi bi-star-fill"></i>
                                                    <i class="bi bi-star-fill"></i>
                                                    <i class="bi bi-star-fill"></i>
                                                </div>
                                                <p class="text-muted">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Itaque reprehenderit accusamus, iste cupiditate quae commodi necessitatibus hic obcaecati sunt recusandae. Tenetur ex veritatis est in sit aspernatur, quasi molestias nam?</p>
                                                <h4>Adele 32, Jogja</h4>
                                            </div>
                                        </div>
                                        <div class="col-md-5 testi-konten d-flex">
                                            <img src={peoplecircle2.png} alt=""/>
                                            <div class="konten-icon ms-4">
                                                <div class="rating text-warning">
                                                    <i class="bi bi-star-fill"></i>
                                                    <i class="bi bi-star-fill"></i>
                                                    <i class="bi bi-star-fill"></i>
                                                    <i class="bi bi-star-fill"></i>
                                                    <i class="bi bi-star-fill"></i>
                                                </div>
                                                <p class="text-muted">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Itaque reprehenderit accusamus, iste cupiditate quae commodi necessitatibus hic obcaecati sunt recusandae. Tenetur ex veritatis est in sit aspernatur, quasi molestias nam?</p>
                                                <h4>John Dee 25, Bromo</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {/* <!--------------Testi item end-------------------> */}
                            </div>                        
                        </div>
                    </div>
                </div>
            </div>
        </section>
        // <!------------------Testimonial ENDS ----------------------->
    )
}

export default Testimonial;
