import React from 'react';
import mobil from '../assets/mobil.png';
import { Services, FooterComp, Slide} from '../components';
import { Link } from "react-router-dom";
import {WhyUs, Testimonial , GettingStarted , Faq} from "../pages";

function Home() {
    return (
        <div>
            <Slide/>
            <Services />
            <WhyUs/>
            <Testimonial/>
            <GettingStarted/>
            <Faq/>
            <FooterComp/>
        </div>

    )
}

export default Home
