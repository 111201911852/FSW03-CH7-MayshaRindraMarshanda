import React from "react";

function WhyUs() {
    return (
        // <!----------Why Us Section START ----------------->
        <section id="why">
            <div class="container-lg py-4">
                <div class="row justify-content-left">
                    <div class="col-lg-8">
                        <div class="isi-tulisan">
                            <h2 class="fw-bold mb-3">Why Us?</h2>
                            <p>Mengapa harus pilih Binar Car Rental?</p>
                        </div>
                    </div>
                </div>
                <div class="row py-4">
                    <div class="col-md-3">
                        <div class="shadow-sm p-4 rounded bg-white border-isi">
                            <div class="icon">
                                <i class="bi bi-hand-thumbs-up"></i>
                            </div>
                            <h5 class="card-title isi-text">Mobil Lengkap</h5>
                            <p class="isi-text-2">Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="shadow-sm p-4 rounded bg-white border-isi">
                            <div class="icon-2 justify-content-left">
                                <i class="bi bi-tag"></i>
                            </div>
                            <h5 class="card-title isi-text">Harga Murah</h5>
                            <p class="isi-text-2">Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="shadow-sm p-4 rounded bg-white border-isi">
                            <div class="icon-3">
                                <i class="bi bi-clock"></i>
                            </div>
                            <h5 class="card-title isi-text">Layanan 24 Jam</h5>
                            <p class="isi-text-2">Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                    <div class="shadow-sm p-4 rounded bg-white border-isi">
                        <div class="icon-4">
                        <i class="bi bi-award"></i>
                        </div>
                        <h5 class="card-title isi-text">Sopir Profesional</h5>
                        <p class="isi-text-2">Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu</p>
                    </div>
                    </div>

                </div>
            </div>
        </section>
    )
}

export default WhyUs;